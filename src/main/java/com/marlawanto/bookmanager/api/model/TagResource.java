package com.marlawanto.bookmanager.api.model;

import com.marlawanto.bookmanager.core.enums.Status;
import lombok.Data;

@Data
public class TagResource {

    private long id;
    private String name;
    private String category;
    private Status status;
    private String createdBy;
    private String updatedBy;
}
