package com.marlawanto.bookmanager.api.controller;

import com.marlawanto.bookmanager.api.model.BookResource;
import com.marlawanto.bookmanager.core.enums.Status;
import com.marlawanto.bookmanager.core.service.BookService;
import com.marlawanto.bookmanager.util.RequestAndParamBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("book")
public class BookController {

    @Autowired
    private BookService bookService;

    @Autowired
    private RequestAndParamBuilder requestAndParamBuilder;

    @GetMapping
    public List<BookResource> get() throws Exception {

        return bookService.getAll().stream()
                .filter(x -> x.getStatus().equals(Status.ACTIVE))
                .map(x -> requestAndParamBuilder.bookParamToResource(x))
                .collect(Collectors.toList());
    }


}
