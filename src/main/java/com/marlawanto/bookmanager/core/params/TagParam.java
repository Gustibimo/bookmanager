package com.marlawanto.bookmanager.core.params;

import com.marlawanto.bookmanager.core.enums.Status;
import lombok.Data;

@Data
public class TagParam {
    private long id;
    private String name;
    private String category;
    private Status status;
    private String createdBy;
    private String updatedBy;
}
