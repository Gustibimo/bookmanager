package com.marlawanto.bookmanager.core.service;

import com.marlawanto.bookmanager.core.params.BookParam;

import java.util.List;

public interface BookService {

    void add(BookParam param);
    void update(BookParam param);
    BookParam getOne(long id) throws Exception;
    List<BookParam> getAll() throws Exception;
    void delete(long id) throws Exception;
}
