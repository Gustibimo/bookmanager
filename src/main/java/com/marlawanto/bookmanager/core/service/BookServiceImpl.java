package com.marlawanto.bookmanager.core.service;

import com.marlawanto.bookmanager.core.entity.Book;
import com.marlawanto.bookmanager.core.enums.Status;
import com.marlawanto.bookmanager.core.params.BookParam;
import com.marlawanto.bookmanager.core.repository.BookRepository;
import com.marlawanto.bookmanager.core.util.ParamBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ParamBuilder paramBuilder;

    @Override
    public void add(BookParam param) {
       param.setStatus(Status.ACTIVE);
       bookRepository.save(paramBuilder.bookParamToEntity(param));
   }

    @Override
    public void update(BookParam param) {
        param.setStatus(Status.ACTIVE);
        bookRepository.save(paramBuilder.bookParamToEntity(param));
    }

    @Override
    public BookParam getOne(long id) throws Exception {
        Book entity = bookRepository.getOne(id);
        if (entity == null) {
            throw new Exception();
        }
        return paramBuilder.bookEntityToParam(entity);
    }

    @Override
    public List<BookParam> getAll() throws Exception {
        List<Book> entities = bookRepository.findAll();
        if (entities == null) {
            throw new Exception();
        }
        return entities.stream()
                .filter(x-> x.getStatus().equals(Status.ACTIVE))
                .map(x->paramBuilder.bookEntityToParam(x))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(long id) throws Exception {
        Book entity = bookRepository.getOne(id);
        if (entity == null) {
            throw new Exception();
        }
        entity.setStatus(Status.DELETED);
        bookRepository.save(entity);
    }

}
