package com.marlawanto.bookmanager.core.enums;

public enum Status {
    ACTIVE,INACTIVE,DELETED;
}
