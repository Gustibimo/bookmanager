FROM java:8
COPY /build/libs/bookmanager-0.0.1-SNAPSHOT.jar bookmanager-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","bookmanager-0.0.1-SNAPSHOT.jar"]